
import os.path
import os
import sys
from numpy import append
from xml.etree.ElementTree import tostring
from fileinput import filename
import glob
import copy
import heapq
from collections import deque
from queue import PriorityQueue
from math import sqrt
import matplotlib.pyplot as plt
from pathlib import Path


inputs = ['input1', 'input2', 'input3', 'input4', 'input5']
algos = ['bfs', 'dfs', 'ucs', 'gbfs', 'astar']
folders = ['level1', 'level2', 'level3']


class node1:
    def __init__(self, parent, pos):
        self.parent = parent
        self.pos = pos


class node2:
    def __init__(self, parent, pos, cost):
        self.parent = parent
        self.pos = pos
        self.cost = cost


class node3:
    def __init__(self, parent, pos, cost,  costPath):
        self.parent = parent
        self.pos = pos
        self.cost = cost
        self. costPath = costPath


class Point:
    def __init__(self, a, b, c):
        self.char = a
        self.value = b
        self.visited = c


class Pos:
    def __init__(self, a, b):
        self.x = a
        self.y = b


class Maze:
    def __init__(self, filename):
        self.maze = []
        file = open(filename, 'r')
        bonus = []
        n = int(file.readline())
        for i in range(0, n):
            tmp = file.readline()
            val = tmp.split(' ')
            bonus.append(val)

        j = 0
        for line in file:
            value = []
            for i in range(0, len(line)):
                if line[i] != '\n':
                    if line[i] == 'x':
                        value.append(Point('X', 0, True))
                    else:
                        if line[i] == 'S':
                            value.append(Point('*', 0, False))
                            self.start = Pos(j, i)
                        else:
                            value.append(Point(' ', 1, False))
            self.maze.append(value)
            j = j+1

        for j in range(0, n):
            self.maze[int(bonus[j][0])][int(bonus[j][1])
                                        ] = Point('+', int(bonus[j][2]), 0)

        self.bonus = []
        for j in range(0, n):
            self.bonus.append(
                [int(bonus[j][0]), int(bonus[j][1]), int(bonus[j][2])])

        for j in range(0, len(self.maze)):
            if self.maze[j][0].char == ' ':
                self.end = Pos(j, 0)
            if self.maze[j][len(self.maze[j])-1].char == ' ':
                self.end = Pos(j, len(self.maze[j])-1)

        for j in range(0, len(self.maze[0])-1):
            if self.maze[0][j].char == ' ':
                self.end = Pos(0, j)
            if self.maze[len(self.maze)-1][j].char == ' ':
                self.end = Pos(len(self.maze) - 1, j)


move = [Pos(-1, 0),  # up
        Pos(1, 0),  # down
        Pos(0, -1),  # left
        Pos(0, 1)]  # right


# Defines the range of movement
#         i-1,j
# i,j-1    i,j    i,j+1
#         i+1,j


dx = [-1, 1, 0, 0]
dy = [0, 0, -1, 1]


def dfs(filename, filePath):
    # time

    myFileName = "dfs"
    map = Maze(filename)
    for input in inputs:
        if input in str(filename):
            filePath = filePath + "/" + input + '/dfs/dfs.txt'
    start = node2("None", map.start, 0)
    now = start

    stack = []
    stack.append(start)

    found = False

    while start:
        now = stack.pop()

        if (now.pos.x == map.end.x and now.pos.y == map.end.y):
            found = True
            break

        new_children = []

        for i in range(0, 4):

            # Move to next position
            neighbor = Pos(now.pos.x + dx[i], now.pos.y + dy[i])

            # Check for valid position

            if (neighbor.x >= 0 and neighbor.y >= 0 and neighbor.x < len(map.maze) and neighbor.y < len(map.maze[0])):
                if (map.maze[neighbor.x][neighbor.y].char != 'x' and map.maze[neighbor.x][neighbor.y].visited == 0):
                    new_node = node2(now, neighbor, now.cost+1)
                    new_children.append(new_node)

        for i in range(0, len(new_children)):
            map.maze[new_children[i].pos.x][new_children[i].pos.y].visited = 1
            stack.append(new_children[i])

    route = []
    if (found == True):
        f = open(filePath, 'w')
        f.write(str(now.cost))
        while now.parent != "None":
            route.insert(0, [now.pos.x, now.pos.y])
            map.maze[now.pos.x][now.pos.y].char = '*'
            now = now.parent
        map.maze[now.pos.x][now.pos.y].char = '*'
        route.insert(0, [start.pos.x, start.pos.y])

        visualize_maze(map, map.bonus, map.start, map.end,
                       myFileName, filename, route)

    else:
        print("null")


def bfs(filename, filePath):

    myFileName = "bfs"
    map = Maze(filename)
    for input in inputs:
        if input in str(filename):
            filePath = filePath + "/" + input + '/bfs/bfs.txt'
    start = node2("None", map.start, 0)
    now = start
    queue = []
    queue.append(start)

    found = False

    while queue:
        now = queue.pop(0)

        if (now.pos.x == map.end.x and now.pos.y == map.end.y):
            found = True
            break
        # get children
        new_children = []

        for i in range(0, 4):

            # Move to next position
            neighbor = Pos(now.pos.x + dx[i], now.pos.y + dy[i])

            # Check for valid position

            if (neighbor.x >= 0 and neighbor.y >= 0 and neighbor.x < len(map.maze) and neighbor.y < len(map.maze[0])):
                if (map.maze[neighbor.x][neighbor.y].char != 'x' and map.maze[neighbor.x][neighbor.y].visited == 0):
                    new_node = node2(now, neighbor, now.cost + 1)
                    new_children.append(new_node)

        for i in range(0, len(new_children)):
            map.maze[new_children[i].pos.x][new_children[i].pos.y].visited = True
            queue.append(new_children[i])

    route = []
    if (found == True):
        f = open(filePath, 'w')
        f.write(str(now.cost))

        while now.parent != "None":
            route.insert(0, [now.pos.x, now.pos.y])
            map.maze[now.pos.x][now.pos.y].char = '*'
            now = now.parent
        map.maze[now.pos.x][now.pos.y].char = '*'
        route.insert(0, [start.pos.x, start.pos.y])

        visualize_maze(map, map.bonus, map.start, map.end,
                       myFileName, filename, route)

    else:
        print("null")


def UCS(filename, filePath):

    myFileName = "ucs"
    map = Maze(filename)
    for input in inputs:
        if input in str(filename):
            filePath = filePath + "/" + input + '/ucs/ucs.txt'
    start = node2("None", map.start, 0)
    now = start
    queue = []
    queue.append(start)

    found = False

    while queue:
        now = queue.pop(0)

        if (now.pos.x == map.end.x and now.pos.y == map.end.y):
            found = True
            break

        # get children
        new_children = []

        for i in range(0, 4):

            # Move to next point
            neighbor = Pos(now.pos.x + dx[i], now.pos.y + dy[i])

            # Check for valid position
            if (neighbor.x >= 0 and neighbor.y >= 0 and neighbor.x < len(map.maze) and neighbor.y < len(map.maze[0])):
                if (map.maze[neighbor.x][neighbor.y].char != 'x' and map.maze[neighbor.x][neighbor.y].visited == 0):

                    new_node = node2(now, neighbor, now.cost + 1)
                    new_children.append(new_node)

        for i in range(0, len(new_children)):
            map.maze[new_children[i].pos.x][new_children[i].pos.y].visited = True
            queue.append(new_children[i])

        queue.sort(key=lambda x: x.cost)

    route = []
    if (found == True):
        f = open(filePath, 'w')
        f.write(str(now.cost))
        while now.parent != "None":
            route.insert(0, [now.pos.x, now.pos.y])
            map.maze[now.pos.x][now.pos.y].char = '*'
            now = now.parent
        map.maze[now.pos.x][now.pos.y].char = '*'
        route.insert(0, [start.pos.x, start.pos.y])

        visualize_maze(map, map.bonus, map.start, map.end,
                       myFileName, filename, route)

    else:
        print("null")


def GBFS(filename, heuristic, filePath):

    myFileName = "gbfs"
    map = Maze(filename)
    for input in inputs:
        if input in str(filename):
            filePath = filePath + "/" + input + '/gbfs/gbfs.txt'
    start = node3("None", map.start, 0, 0)
    now = start
    queue = []
    queue.append(start)

    found = False

    while queue:

        now = queue.pop(0)

        if (now.pos.x == map.end.x and now.pos.y == map.end.y):
            print(now.cost)
            f = open(filePath, 'w')
            f.write(str(now.cost))
            found = True
            break

        # get children
        new_children = []

        for i in range(0, 4):

            # Move to next point
            neighbor = Pos(now.pos.x + dx[i], now.pos.y + dy[i])

            # Check for valid position
            if (neighbor.x >= 0 and neighbor.y >= 0 and neighbor.x < len(map.maze) and neighbor.y < len(map.maze[0])):
                if (map.maze[neighbor.x][neighbor.y].char != 'x' and map.maze[neighbor.x][neighbor.y].visited == 0):

                    g = map.maze[neighbor.x][neighbor.y].value + now.cost
                    # Manhattan Heuristic Calculation -> h = |xstart - xdestination| + |ystart - ydestination|
                    if (heuristic == "manhattan"):
                        h = abs((neighbor.x - map.end.x)) + \
                            abs((neighbor.y - map.end.y))

                    # Euclidean Heuristic Calculation -> h = sqrt((xstart - xdestination)^2 + (ystart - ydestination)^2)
                    if (heuristic == "euclidean"):
                        h = sqrt(((neighbor.x - map.end.x) ** 2) +
                                 ((neighbor.x - map.end.y) ** 2))

                    new_node = node3(now, neighbor, g, h)
                    new_children.append(new_node)

        for i in range(0, len(new_children)):
            map.maze[new_children[i].pos.x][new_children[i].pos.y].visited = True
            queue.append(new_children[i])

        queue.sort(key=lambda x: x. costPath)

    route = []
    if (found == True):

        while now.parent != "None":
            route.insert(0, [now.pos.x, now.pos.y])
            map.maze[now.pos.x][now.pos.y].char = '*'
            now = now.parent
        map.maze[now.pos.x][now.pos.y].char = '*'
        route.insert(0, [start.pos.x, start.pos.y])

        visualize_maze(map, map.bonus, map.start, map.end,
                       myFileName, filename, route)

    else:
        print("null")


def astar(filename, heuristic, filePath):

    map = Maze(filename)
    myFileName = "astar"
    for input in inputs:
        if input in str(filename):
            filePath = filePath + "/" + input + '/astar/astar.txt'
    start = node3("None", map.start, 0, 0)
    now = start
    queue = []
    queue.append(start)

    found = False

    while queue:

        now = queue.pop(0)

        if (now.pos.x == map.end.x and now.pos.y == map.end.y):
            found = True
            break

        new_children = []

        for i in range(0, 4):

            # Move to next point
            neighbor = Pos(now.pos.x + dx[i], now.pos.y + dy[i])

            # Check for valid position
            if (neighbor.x >= 0 and neighbor.y >= 0 and neighbor.x < len(map.maze) and neighbor.y < len(map.maze[0])):
                if (map.maze[neighbor.x][neighbor.y].char != 'x' and map.maze[neighbor.x][neighbor.y].visited == 0):

                    g = map.maze[neighbor.x][neighbor.y].value + now.cost

                    # Manhattan Heuristic Calculation -> h = |xstart - xdestination| + |ystart - ydestination|
                    if (heuristic == "manhattan"):
                        h = abs((neighbor.x - map.end.x)) + \
                            abs((neighbor.y - map.end.y))

                    # Euclidean Heuristic Calculation -> h = sqrt((xstart - xdestination)^2 + (ystart - ydestination)^2)
                    if (heuristic == "euclidean"):
                        h = sqrt(((neighbor.x - map.end.x) ** 2) +
                                 ((neighbor.x - map.end.y) ** 2))

                    new_node = node3(now, neighbor, g, g+h)
                    new_children.append(new_node)

        for i in range(0, len(new_children)):
            map.maze[new_children[i].pos.x][new_children[i].pos.y].visited = True
            queue.append(new_children[i])

        queue.sort(key=lambda x: x. costPath)

    route = []
    if (found == True):
        f = open(filePath, 'w')
        f.write(str(now. costPath))
        while now.parent != "None":
            route.insert(0, [now.pos.x, now.pos.y])
            map.maze[now.pos.x][now.pos.y].char = '*'
            now = now.parent
        map.maze[now.pos.x][now.pos.y].char = '*'
        route.insert(0, [start.pos.x, start.pos.y])

        visualize_maze(map, map.bonus, map.start, map.end,
                       myFileName, filename, route)

    else:
        print("null")


def visualize_maze(matrix, bonus, start, end, myFileName, filename, route=None):
    """
    Args:
      1. matrix: The matrix read from the input file,
      2. bonus: The array of bonus points,
      3. start, end: The starting and ending points,
      4. route: The route from the starting point to the ending one, defined by an array of (x, y), e.g. route = [(1, 2), (1, 3), (1, 4)]
    """
    # 1. Define walls and array of direction based on the route
    walls = [(i, j) for i in range(len(matrix.maze))
             for j in range(len(matrix.maze[0])) if matrix.maze[i][j].char == 'X']

    if route:
        direction = []
        for i in range(1, len(route)):
            if route[i][0]-route[i-1][0] > 0:
                direction.append('v')  # ^
            elif route[i][0]-route[i-1][0] < 0:
                direction.append('^')  # v
            elif route[i][1]-route[i-1][1] > 0:
                direction.append('>')
            else:
                direction.append('<')

        direction.pop(0)

    # 2. Drawing the map
    ax = plt.figure(dpi=100).add_subplot(111)

    for i in ['top', 'bottom', 'right', 'left']:
        ax.spines[i].set_visible(False)

    plt.scatter([i[1] for i in walls], [-i[0] for i in walls],
                marker='X', s=100, color='black')

    plt.scatter([i[1] for i in bonus], [-i[0] for i in bonus],
                marker='P', s=100, color='green')

    plt.scatter(start.y, -start.x, marker='*',
                s=100, color='gold')

    if route:
        for i in range(len(route)-2):
            plt.scatter(route[i+1][1], -route[i+1][0],
                        marker=direction[i], color='silver')

    plt.text(end.y, -end.x, 'EXIT', color='red',
             horizontalalignment='center',
             verticalalignment='center')
    plt.xticks([])
    plt.yticks([])
    levels = ['level1', 'level2']
    for input in inputs:
        for algo in algos:
            for level in levels:
                if (input in str(filename) and level in str(filename) and algo in myFileName):
                    plt.savefig('output/'+level + '/'+input+'/' +
                                myFileName + '/' + myFileName+'.jpg')

    print(f'Starting point (x, y) = {start.x, start.y}')
    print(f'Ending point (x, y) = {end.x, end.y}')

    for _, point in enumerate(bonus):
        print(
            f'Bonus point at position (x, y) = {point[0], point[1]} with point {point[2]}')


# MAIN
# get working directory
# path = os.getcwd()
# print(path)
file_path = os.path.abspath(os.path.dirname(__file__))
path = str(file_path).replace('\source', '')
print(path)

# print(os.listdir('input'))
# Parent Directories
output_dir = path + '/output'
output_level1_dir = path + '/output/level1'
output_level2_dir = path + '/output/level2'


# TAO FOLDER
output_level1_inputs_dir = []
for input in inputs:
    output_level1_inputs_dir.append(path + '/output/level1/' + input)

for folder in folders:
    os.makedirs(os.path.join(output_dir, folder))
for input in inputs:
    os.mkdir(os.path.join(output_level1_dir, input))

for algo in algos:
    for input in output_level1_inputs_dir:
        os.mkdir(os.path.join(input, algo))


# LAY  DUONG DAN FILE INPUT
input_data_folder = Path(path + "/input/level1")
inputs_level1_dir = []
for input in inputs:
    inputs_level1_dir.append(
        Path(path + "/input/level1" + "/" + input + ".txt"))
for input_file in inputs_level1_dir:
    astar(input_file, 'manhattan', output_level1_dir)
    GBFS(input_file, 'euclidean', output_level1_dir)
    dfs(input_file, output_level1_dir)
    bfs(input_file, output_level1_dir)
    UCS(input_file, output_level1_dir)


# LEVEL 2 FOLDER
inputs_level2 = ['input1', 'input2', 'input3']
output_level2_inputs_dir = []
for input in inputs_level2:
    output_level2_inputs_dir.append(path + '/output/level2/' + input)

for input in inputs_level2:
    os.mkdir(os.path.join(output_level2_dir, input))

for algo in algos:
    for input in output_level2_inputs_dir:
        os.mkdir(os.path.join(input, algo))

inputs_level2_dir = []
for input in inputs_level2:
    inputs_level2_dir.append(
        Path(path + "/input/level2" + "/" + input + ".txt"))
for input_file in inputs_level2_dir:
    astar(input_file, 'manhattan', output_level2_dir)
    GBFS(input_file, 'manhattan', output_level2_dir)
    dfs(input_file, output_level2_dir)
    bfs(input_file, output_level2_dir)
    UCS(input_file, output_level2_dir)
